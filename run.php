<?php
$input = $argv[1];
if (!isset($input)) exit ('Строка не задана!'."\n");

$left = $right = $brackets = 0; //brackets - количество пар скобок
for ($i=0; $i<strlen($input); $i++){
    $symbol = $input[$i];

    if ($symbol=='(') $left++;

    if ($symbol ==')'){
        if ($left>0){
            $left--;
            $brackets++;
        } else{
            $right++;
        }
    }
}

if ($left>0 or $right>0) exit("🛑 Количество открывающихся и закрывающихся скобок не совпадает!\n") ;
if ($brackets==0) exit('⚠️️️ В строке не найдены скобки.');
echo "✅ Количество открывающихся и закрывающихся скобок совпадает.\n";