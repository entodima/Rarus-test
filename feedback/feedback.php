<?php
$username = 'root';
$password = 'root';
$dsn = 'mysql:dbname=test;host=127.0.0.1;port=3306';

try{
    $pdo = new PDO($dsn, $username, $password);
}catch (PDOException $exception){
    echo json_encode(['success' => 3]);
    exit;
}

if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['phone']) && isset($_POST['comment']) && isset($_FILES['file'])) {
    if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) and preg_match("/^[0-9]{11}$/",$_POST['phone'])){
        $sql = "INSERT INTO `test` (`name`, `email`,`phone`, `comment`) VALUES (".$_POST['name'].", ".$_POST['email'].", ".$_POST['phone'].", ".$_POST['comment'].")";
        
        $source = $_FILES["image"]["tmp_name"];
        $dest = "images/".$_FILES["image"]["name"];

        if (move_uploaded_file($source, $dest)) {
            if($pdo->query($sql)->execute()) echo json_encode(['success' => 1]);
            else $error_code = 0;
        } else $error_code = 0;

    } else $error_code = 2;
} else $error_code = 0;

echo json_encode(['success' => $error_code]);
